# Catch-The-Kenny

A simple game where the goal is to be fast enough to tap on the Kenny icon as many times as possible within a time interval. The Kenny object appears at random places and disappears quickly. Highest score is saved locally and users are notified when they do better than it.

Demo Video : https://www.dropbox.com/s/00xowzvpwzlaqhw/7-CatchTheKenny.mp4?dl=0