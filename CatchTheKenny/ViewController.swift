//
//  ViewController.swift
//  CatchTheKenny
//
//  Created by Ibraheem Hindi on 8/4/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var time_label: UILabel!
    @IBOutlet weak var score_label: UILabel!
    @IBOutlet weak var best_score_label: UILabel!
    
    @IBOutlet weak var k1: UIImageView!
    @IBOutlet weak var k2: UIImageView!
    @IBOutlet weak var k3: UIImageView!
    @IBOutlet weak var k4: UIImageView!
    @IBOutlet weak var k5: UIImageView!
    @IBOutlet weak var k6: UIImageView!
    @IBOutlet weak var k7: UIImageView!
    @IBOutlet weak var k8: UIImageView!
    @IBOutlet weak var k9: UIImageView!
    
    let interval = 1
    var timer = Timer()
    var time = 10
    var score = 0
    var visible = 3
    var kennys = [UIImageView]()
    
    func randomNumber(from:Int, to:Int) -> Int{
        if from == to{
            return from
        }
        else if from > to{
            return -1
        }
        else{
            return Int(arc4random_uniform(UInt32(to-from+1))) + from
        }
    }
    
    func runTimer(){
        self.time = 10
        time_label.text = "Time : \(time)"
        
        self.score = 0
        score_label.text = "Score : \(score)"
        
        self.visible = 3
        
        timer = Timer.scheduledTimer(
            timeInterval: Double(interval),
            target: self,
            selector: #selector(timerUpdate),
            userInfo: nil,
            repeats: true
        )
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Restart", style: .cancel) {UIAlertAction in
            self.kennys[self.visible - 1].alpha = 0
            self.kennys[2].alpha = 1
            self.runTimer()
        })
        
        alert.addAction(UIAlertAction(title: "Quit", style: .default) {UIAlertAction in
            exit(0)
        })
        
        self.present(alert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        kennys.append(k1)
        kennys.append(k2)
        kennys.append(k3)
        kennys.append(k4)
        kennys.append(k5)
        kennys.append(k6)
        kennys.append(k7)
        kennys.append(k8)
        kennys.append(k9)
        
        for item in kennys{
            item.isUserInteractionEnabled = true
            let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(onTap))
            item.addGestureRecognizer(tap_recognizer)
        }
        
        if let best_score_obj = UserDefaults.standard.object(forKey: "best_score"){
            let best_score = best_score_obj as! Int
            best_score_label.text = "Best Score : \(best_score)"
        }
        else{
            updateBestScore(new_score: 0)
        }
        
        runTimer()
    }
    
    @objc func onTap(){
        score += 1
        score_label.text = "Score : \(score)"
    }
    
    func updateBestScore(new_score: Int){
        UserDefaults.standard.set(new_score, forKey: "best_score")
        UserDefaults.standard.synchronize()
    }
    
    @objc func timerUpdate(sender: Timer){
        if time > 0{
            let rand = randomNumber(from: 1, to: 9)
            
            UIView.animate(withDuration: 0.35) {
                self.kennys[self.visible - 1].alpha = 0
                self.kennys[rand - 1].alpha = 1
            }
            
            visible = rand
            time -= interval
            
            if time >= 0{
                time_label.text = "Time : \(time)"
            }
        }
        else{
            timer.invalidate()
            var high_score = ""
            
            if let best_score_obj = UserDefaults.standard.object(forKey: "best_score"){
                let best_score = best_score_obj as! Int
                if score > best_score{
                    high_score = "New high score. "
                    best_score_label.text = "Best Score : \(score)"
                    updateBestScore(new_score: score)
                }
            }
            else{
                updateBestScore(new_score: score)
            }
            
            showAlert(title: "Game Over", message: "\(high_score)You scored \(score)")
        }
    }

}















